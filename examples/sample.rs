use easy_bitfield::*;

type A = BitField<u32, bool, 0, 1, false>;
type B = BitField<u32, i8, { A::NEXT_BIT }, 4, true>;
type C = BitField<u32, i16, { B::NEXT_BIT }, 16, true>;

fn main() {
    let mut storage;

    storage = A::encode(true);
    assert!(A::decode(storage));
    storage = B::update(-1, storage);
    assert_eq!(B::decode(storage), -1);
    storage = B::update(2, storage);
    assert_eq!(B::decode(storage), 2);
    assert!(A::decode(storage));
    assert_eq!(C::decode(storage), 0);
    assert!(B::is_valid(7));
    assert!(!B::is_valid(8));
    assert!(B::is_valid(-8));
    assert!(!B::is_valid(-9));

    println!("Creating atomic bitfield storage");

    type LockBit = BitField<usize, bool, 0, 1, false>;
    type DataBits = BitField<usize, u32, { LockBit::NEXT_BIT }, 32, false>;

    let container = std::sync::Arc::new(AtomicBitfieldContainer::new(0usize));
    println!(
        "lock bit: {}, data bits: {}",
        container.read::<LockBit>(),
        container.read::<DataBits>()
    );
    let thread = {
        let container = container.clone();

        std::thread::spawn(move || {
            println!("thread 1 spawned");
            while !container.try_acquire::<LockBit>() {}
            container.update::<DataBits>(42);
            println!(
                "thread 1: set data bits to {}",
                container.read::<DataBits>()
            );
            assert!(container.try_release::<LockBit>());
        })
    };

    loop {
        while !container.try_acquire::<LockBit>() {
            std::thread::yield_now();
        }
        if container.read::<DataBits>() != 0 {
            println!("data bits is not zero, break");
            assert_eq!(container.read::<DataBits>(), 42);
            assert!(container.try_release::<LockBit>());
            break;
        }

        println!("data bits is zero, loop");

        assert!(container.try_release::<LockBit>());
    }
    println!("{}", container.read::<DataBits>());
    thread.join().unwrap();
}
