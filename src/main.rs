use easy_bitfield::*;

pub type X = BitField<usize, bool, 0, 1, false>;
pub type Y = BitField<usize, u16, 1, 16, false>;
pub type Z = BitField<usize, i32, { Y::NEXT_BIT }, 32, true>;

fn main() {
    let storage = AtomicBitfieldContainer::new(0);

    storage.update::<X>(true);
    storage.update::<Y>(42);
    storage.update::<Z>(-42);

    println!("{:x}", storage.load_ignore_race());

    println!("{}", storage.read::<Z>());
    println!("{}", storage.read::<Y>());
    println!("{}", storage.read::<X>());
}
